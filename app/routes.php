<?php

$container = $app->getContainer();

// Private urls
$app->group('', function(){
  $this->get('/', 'HomeController:index');
})
  ->add( new \App\Middleware\Account\CheckPermission( $container ) )
  //->add( new \App\Middleware\Account\SetDatabase )
;


// ->add( new \App\Middleware\Account\CheckPermission );
