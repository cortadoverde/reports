<?php

use Illuminate\Database\Capsule\Manager as Capsule;

$container = $app->getContainer();

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['mongodb']);
$capsule->getDatabaseManager()->extend('mongodb', function($config) {
    return new Jenssegers\Mongodb\Connection($config);
});
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['mongodb'] = function( $container ) use ( $capsule ){
  return $capsule;
};
