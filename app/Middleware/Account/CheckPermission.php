<?php

namespace App\Middleware\Account;

use App\Middleware\BaseMiddleware;

Class CheckPermission extends BaseMiddleware
{

  public function __invoke( $request, $response, $next )
  {

      if( ! isset( $_SESSION['user'] ) ) {
        $response = new \Slim\Http\Response();
        $this->view->render( $response, 'app/error/401.twig', ['content' => 'Necesita autenticarse']);
        return $response;
      }

      return $next($request, $response);
  }

}
