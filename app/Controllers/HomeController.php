<?php

namespace App\Controllers;

use App\Models\Track;

class HomeController extends Controller
{

  use Traits\ThemePage;

  public function index( $request, $response )
  {

    $data = $this->container->mongodb->table('tracks')->get();

    // Ahora se deberia trackear como cortadoverde.reports
    // Esta saliendo el real path en el nombre del proyecto
    // Probando ahora a ver como mape

    // Traits\ThemePage::setTheme()
    $this->setTheme('lod');
    $this->loadTheme();

    return $this->view->render($response, 'home.twig');
  }


}
