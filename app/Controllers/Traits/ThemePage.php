<?php
namespace App\Controllers\Traits;

trait ThemePage
{

    protected $themePath = \App\Config::PATH_RESOURCES . '/themes/';
    protected $theme = 'default';

    public function loadTheme()
    {
      $this->container->view->getEnvironment()->addGlobal( 'theme' , $this->theme );
    }

    public function setTheme( string $themeName )
    {
      $this->theme = $themeName;
    }

    public function setThemePath( string $path )
    {
      $this->themePath = $path;
    }

}
