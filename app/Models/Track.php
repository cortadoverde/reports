<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Track extends Eloquent {
  protected $table = 'tracks';
}
